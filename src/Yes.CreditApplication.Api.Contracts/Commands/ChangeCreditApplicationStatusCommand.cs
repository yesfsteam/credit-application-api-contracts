﻿using System;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;

namespace Yes.CreditApplication.Api.Contracts.Commands
{
    [Obsolete]
    public class ChangeCreditApplicationStatusCommand : CreditApplicationResponse
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
    }
}