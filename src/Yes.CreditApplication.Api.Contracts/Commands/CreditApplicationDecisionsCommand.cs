﻿using System;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;

namespace Yes.CreditApplication.Api.Contracts.Commands
{
    public class CreditApplicationDecisionsCommand : CreditApplicationResponse
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
    }
}