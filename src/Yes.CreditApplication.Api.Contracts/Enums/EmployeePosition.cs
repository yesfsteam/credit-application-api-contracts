﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum EmployeePosition
    {
        [Description("Рабочий")]
        Employee = 1,
        [Description("Специалист")]
        Specialist = 2,
        [Description("Менеджер")]
        Manager = 3,
        [Description("Руководитель")]
        Head = 4
    }
}