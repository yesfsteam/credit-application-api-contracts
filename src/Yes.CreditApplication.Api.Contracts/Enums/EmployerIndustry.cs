﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum EmployerIndustry
    {
        [Description("Банки, управляющие компании, финансы")]
        Banks = 1,
        [Description("Благотворительные и религиозные организации")]
        CharityAndReligiousOrganizations = 2,
        [Description("ВПК")]
        Military = 3,
        [Description("Государственная служба")]
        CivilService = 4,
        [Description("Добывающая промышленность")]
        Mining = 5,
        [Description("Здравоохранение")]
        Healthcare = 6,
        [Description("Игорный бизнес")]
        Gambling = 7,
        [Description("Информационные технологии")]
        InformationTechnology = 8,
        [Description("Наука, образование")]
        ScienceEducation = 9,
        [Description("Подбор персонала")]
        HumanResources = 10,
        [Description("Производство")]
        Manufacturing = 11,
        [Description("Реклама, PR, маркетинг")]
        Marketing = 12,
        [Description("Ресторанный бизнес")]
        Restaurants = 13,
        [Description("Сельское хозяйство")]
        Agriculture = 14,
        [Description("Страхование")]
        Insurance = 15,
        [Description("Строительство, недвижимость")]
        Development = 16,
        [Description("Торговля")]
        Trading = 17,
        [Description("Транспорт, логистика")]
        Transport = 18,
        [Description("Туризм, развлечения")]
        Tourism = 19,
        [Description("Юридические услуги")]
        LegalServices = 20,
        [Description("Другое")]
        Other = 21
    }
}