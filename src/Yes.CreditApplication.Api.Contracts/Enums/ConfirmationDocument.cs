﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum ConfirmationDocument
    {
        [Description("Справка по форме 2-НДФЛ")]
        IncomeStatement = 1,
        [Description("Справка о доходах с печатью работодателя")]
        EmployerStampedIncomeStatement = 2,
        [Description("Выписка с банковского счета")]
        BankStatement = 3,
        [Description("Выписка с Пенсионного фонда РФ")]
        PensionFundBankStatement = 4,
        [Description("Нет подтверждающего документа")]
        NoDocument = 5
    }
}