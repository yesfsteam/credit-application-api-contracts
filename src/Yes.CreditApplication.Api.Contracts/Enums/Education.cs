﻿using System.ComponentModel;

namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum Education
    {
        [Description("Начальная школа")]
        PrimarySchool = 1,
        [Description("Средняя школа")]
        HighSchool = 2,
        [Description("Специализированная средняя школа")]
        SpecializedHighSchool = 3,
        [Description("Незаконченное среднее образование")]
        IncompleteSecondaryEducation = 4,
        [Description("Незаконченное высшее образование")]
        IncompleteHigherEducation = 5,
        [Description("Высшее образование")]
        HigherEducation = 6,
        [Description("Два и более высших образования")]
        TwoOrMoreHigherEducations = 7,
        [Description("Ученая степень")]
        AcademicDegree = 8
    }
}