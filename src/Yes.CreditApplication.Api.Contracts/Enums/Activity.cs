﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum Activity
    {
        [Description("Наемный работник")]
        Employee = 1,
        [Description("Самозанятый/ИП")]
        SelfEmployed = 2,
        [Description("Не работаю")]
        Unemployed = 3
    }
}