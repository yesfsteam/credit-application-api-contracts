﻿using System.ComponentModel;

namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum Gender
    {
        [Description("Мужской")]
        Male = 1,
        [Description("Женский")]
        Female = 2
    }
}