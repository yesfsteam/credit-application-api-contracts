﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum AdditionalPhoneNumberType
    {
        [Description("Супруг/Супруга")]
        Spouse = 1,
        [Description("Мать")]
        Mother = 2,
        [Description("Отец")]
        Father = 3,
        [Description("Брат")]
        Brother = 4,
        [Description("Сестра")]
        Sister = 5,
        [Description("Сын")]
        Son = 6,
        [Description("Дочь")]
        Daughter = 7,
        [Description("Бабушка")]
        Grandmother = 8,
        [Description("Дедушка")]
        Grandfather = 9,
        [Description("Друг")]
        Friend = 10,
        [Description("Коллега")]
        Colleague = 11,
        [Description("Другое")]
        Other = 12
    }
}