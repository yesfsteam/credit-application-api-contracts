﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum CreditApplicationStep
    {
        [Description("Введена основная информация")]
        MainInformation = 1,
        [Description("Телефон подтвержден")]
        ConfirmationCode = 2,
        [Description("Введена дополнительная информация")]
        AdditionalInformation = 3,
        [Description("Введена информация о работодателе")]
        EmployerInformation = 4
    }
}