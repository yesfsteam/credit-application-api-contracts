﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum CreditApplicationStatus
    {
        [Description("Черновик")]
        Draft = 1,
        [Description("Новая")]
        New = 2,
        [Description("В работе")]
        InProgress = 3,
        [Description("Недозвон")]
        UnableToContact = 4,
        [Description("Обработана")]
        Processed = 5,
        [Description("Отказ")]
        Rejected = 6,
        [Description("Самоотказ")]
        ClientRejected = 7,
        [Description("Перезвонить")]
        NeedToCallBack = 8,
        [Description("Недозвон-архив")]
        UnableToContactArchive = 9,
        [Description("Техническая проблема")]
        TechnicalProblem = 10
    }
}