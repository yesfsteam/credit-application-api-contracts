﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum MaritalStatus
    {
        [Description("Не женат/Не замужем")]
        Single = 1,
        [Description("Женат/Замужем")]
        Married = 2,
        [Description("Вдовец/Вдова")]
        Widowed = 3,
        [Description("В разводе/Проживает раздельно")]
        Divorced = 4,
        [Description("Повторный брак")]
        Remarriage = 5,
        [Description("Гражданский брак/Совместное проживание")]
        CivilMarriage = 6
    }
}