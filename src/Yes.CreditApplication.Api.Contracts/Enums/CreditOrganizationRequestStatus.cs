﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum CreditOrganizationRequestStatus
    {
        [Description("В обработке")]
        Pending = 1,
        [Description("Одобрено")]
        Approved = 2,
        [Description("Отказ")]
        Declined = 3,
        [Description("Ошибка валидации")] 
        ValidationErrorCheck = 4,
        [Description("Ошибка авторизации")] 
        AuthorizaionErrorCheck = 5,
        [Description("Ошибка")]
        ServerErrorCheck = 6,
        [Description("Заявка подтверждена")] 
        Confirmed = 7,
        [Description("Заявка отклонена")] 
        Rejected = 8,
        [Description("Ошибка валидации")] 
        ValidationErrorConfirm = 9,
        [Description("Ошибка авторизации")] 
        AuthorizaionErrorConfirm = 10,
        [Description("Ошибка")]
        ServerErrorConfirm = 11,
        [Description("")]
        None = 12,
        [Description("Не проходит по условиям")]
        NotApplicable = 13
    }
}