﻿﻿using System.ComponentModel;

 namespace Yes.CreditApplication.Api.Contracts.Enums
{
    public enum ProfileType
    {
        [Description("Полная")]
        Full = 1,
        [Description("Средняя")]
        Medium = 2,
        [Description("Короткая")]
        Short = 3
    }
}