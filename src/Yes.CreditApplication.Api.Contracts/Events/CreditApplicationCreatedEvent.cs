﻿using System;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;

namespace Yes.CreditApplication.Api.Contracts.Events
{
    public class CreditApplicationCreatedEvent : CreditApplicationRequest, IRetryMessage
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата создания заявки на кредит
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Номер попытки обаботки события
        /// </summary>
        public int RetryCount { get; set; }
    }
}