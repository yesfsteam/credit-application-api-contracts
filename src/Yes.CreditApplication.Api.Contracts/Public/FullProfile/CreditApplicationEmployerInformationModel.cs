﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Public.FullProfile
{
    public class CreditApplicationEmployerInformationModel : JsonModel
    {
        /// <summary>
        /// Вид деятельности
        /// </summary>
        public Activity Activity { get; set; }
        
        /// <summary>
        /// ИНН
        /// </summary>
        public string Tin { get; set; }
        
        /// <summary>
        /// Доход в месяц
        /// </summary>
        public int MonthlyIncome { get; set; }
        
        /// <summary>
        /// Наименование работодателя
        /// </summary>
        public string EmployerName { get; set; }
        
        /// <summary>
        /// Регион работодателя
        /// </summary>
        public string EmployerAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город работодателя
        /// </summary>
        public string EmployerAddressCity { get; set; }
        
        /// <summary>
        /// Код города работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица работодателя
        /// </summary>
        public string EmployerAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом работодателя
        /// </summary>
        public string EmployerAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус работодателя
        /// </summary>
        public string EmployerAddressBlock { get; set; }
        
        /// <summary>
        /// Строение работодателя
        /// </summary>
        public string EmployerAddressBuilding { get; set; }
        
        /// <summary>
        /// Офис работодателя
        /// </summary>
        public string EmployerAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressKladrCode { get; set; }
        
        /// <summary>
        /// Отрасль работодателя
        /// </summary>
        public EmployerIndustry? EmployerIndustry { get; set; }
        
        /// <summary>
        /// Численность работников
        /// </summary>
        public int? EmployerStaff { get; set; }
        
        /// <summary>
        /// Рабочий телефон
        /// </summary>
        public string EmployerPhoneNumber { get; set; }
        
        /// <summary>
        /// Дата начала работы на текущем месте
        /// </summary>
        public DateTime? EmployeeStartDate { get; set; }
        
        /// <summary>
        /// Должность
        /// </summary>
        public EmployeePosition? EmployeePosition { get; set; }
    }
}