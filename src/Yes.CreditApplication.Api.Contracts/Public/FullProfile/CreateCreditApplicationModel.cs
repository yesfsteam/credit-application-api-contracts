﻿using System;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Public.FullProfile
{
    public class CreateCreditApplicationModel : JsonModel
    {
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int CreditAmount { get; set; }
        
        /// <summary>
        /// Срок кредита (в днях)
        /// </summary>
        public int CreditPeriod { get; set; }
        
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// E-mail клиента
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Признак, означающий согласие клиента на обработку персональных данных и отправку данных в БКИ
        /// </summary>
        public bool ClientConsentReceived { get; set; }
        
        /// <summary>
        /// Идентификатор партнера
        /// </summary>
        public Guid PartnerId { get; set; }
        
        /// <summary>
        /// IP адрес запроса на создание заявки на кредит
        /// </summary>
        public string RequestIp { get; set; }
    }
}