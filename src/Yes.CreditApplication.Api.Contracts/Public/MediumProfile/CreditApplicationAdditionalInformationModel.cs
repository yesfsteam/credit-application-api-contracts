﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Public.MediumProfile
{
    public class CreditApplicationAdditionalInformationModel : JsonModel
    {
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }
        
        /// <summary>
        /// Место рождения
        /// </summary>
        public string PlaceOfBirth { get; set; }
        
        /// <summary>
        /// Пол
        /// </summary>
        public Gender Gender { get; set; }
        
        /// <summary>
        /// Доход в месяц
        /// </summary>
        public int MonthlyIncome { get; set; }
        
        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string PassportSeries { get; set; }
        
        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string PassportNumber { get; set; }
        
        /// <summary>
        /// Кем выдан
        /// </summary>
        public string PassportIssuer { get; set; }
        
        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime PassportIssueDate { get; set; }
        
        /// <summary>
        /// Код подразделения
        /// </summary>
        public string PassportDepartmentCode { get; set; }
        
        /// <summary>
        /// Регион прописки
        /// </summary>
        public string RegistrationAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город прописки
        /// </summary>
        public string RegistrationAddressCity { get; set; }
        
        /// <summary>
        /// Код города прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица прописки
        /// </summary>
        public string RegistrationAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом прописки
        /// </summary>
        public string RegistrationAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус прописки
        /// </summary>
        public string RegistrationAddressBlock { get; set; }
        
        /// <summary>
        /// Строение прописки
        /// </summary>
        public string RegistrationAddressBuilding { get; set; }
        
        /// <summary>
        /// Квартира прописки
        /// </summary>
        public string RegistrationAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressKladrCode { get; set; }
        
        /// <summary>
        /// Регион проживания
        /// </summary>
        public string ResidenceAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город проживания
        /// </summary>
        public string ResidenceAddressCity { get; set; }
        
        /// <summary>
        /// Код города проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица проживания
        /// </summary>
        public string ResidenceAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом проживания
        /// </summary>
        public string ResidenceAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус проживания
        /// </summary>
        public string ResidenceAddressBlock { get; set; }
        
        /// <summary>
        /// Строение проживания
        /// </summary>
        public string ResidenceAddressBuilding { get; set; }
        
        /// <summary>
        /// Квартира проживания
        /// </summary>
        public string ResidenceAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressKladrCode { get; set; }
    }
}