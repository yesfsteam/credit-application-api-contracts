﻿using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Public
{
    public class CreditApplicationConfirmationCodeModel : JsonModel
    {
        /// <summary>
        /// Код подтверждения
        /// </summary>
        public string ConfirmationCode { get; set; }
    }
}