﻿using System;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Public.ShortProfile
{
    public class CreateCreditApplicationModel : JsonModel
    {
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }
        
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Дополнительный телефон
        /// </summary>
        public string AdditionalPhoneNumber { get; set; }
        
        /// <summary>
        /// Регион проживания
        /// </summary>
        public string ResidenceAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Признак, означающий согласие клиента на обработку персональных данных и отправку данных в БКИ
        /// </summary>
        public bool ClientConsentReceived { get; set; }
        
        /// <summary>
        /// Идентификатор партнера
        /// </summary>
        public Guid PartnerId { get; set; }
        
        /// <summary>
        /// IP адрес запроса на создание заявки на кредит
        /// </summary>
        public string RequestIp { get; set; }
    }
}