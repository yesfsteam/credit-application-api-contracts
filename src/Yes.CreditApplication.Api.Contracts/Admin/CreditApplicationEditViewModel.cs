﻿using Yes.CreditApplication.Api.Contracts.Enums;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationEditViewModel : UpdateCreditApplicationModel
    {
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Статус
        /// </summary>
        public CreditApplicationStatus Status { get; set; }

        /// <summary>
        /// Возвращет значение, указан ли адрес регистрации
        /// </summary>
        public bool IsRegistrationAddressEmpty()
        {
            return string.IsNullOrWhiteSpace(RegistrationAddressRegion) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressCity) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressStreet) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressHouse) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressBlock) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressBuilding) &&
                   string.IsNullOrWhiteSpace(RegistrationAddressApartment);
        }
        
        /// <summary>
        /// Возвращет значение, указан ли адрес проживания
        /// </summary>
        public bool IsResidenceAddressEmpty()
        {
            return string.IsNullOrWhiteSpace(ResidenceAddressRegion) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressCity) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressStreet) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressHouse) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressBlock) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressBuilding) &&
                   string.IsNullOrWhiteSpace(ResidenceAddressApartment);
        }
        
        /// <summary>
        /// Адрес проживания совпадает с адресом регистрации
        /// </summary>
        public bool ResidenceAddressEquals()
        {
            return !IsRegistrationAddressEmpty() && 
                   !IsResidenceAddressEmpty() &&
                   RegistrationAddressRegion == ResidenceAddressRegion &&
                   RegistrationAddressCity == ResidenceAddressCity &&
                   RegistrationAddressStreet == ResidenceAddressStreet &&
                   RegistrationAddressHouse == ResidenceAddressHouse &&
                   RegistrationAddressBlock == ResidenceAddressBlock &&
                   RegistrationAddressBuilding == ResidenceAddressBuilding &&
                   RegistrationAddressApartment == ResidenceAddressApartment;
        }

        
    }
}