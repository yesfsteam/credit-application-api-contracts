﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationListItemModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата заявки на кредит
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int? CreditAmount { get; set; }
        
        /// <summary>
        /// Срок кредита (в месяцах)
        /// </summary>
        public int? CreditPeriod { get; set; }
        
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// E-mail клиента
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Статус заявки
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
    }
}