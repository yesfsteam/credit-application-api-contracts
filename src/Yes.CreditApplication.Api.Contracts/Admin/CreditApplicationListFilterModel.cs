﻿using System;
using System.Collections.Generic;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationListFilterModel : JsonModel
    {
        /// <summary>
        /// Пропустить Skip элементов
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// Количество элементов для отображения
        /// </summary>
        public int Take { get; set; }
        
        /// <summary>
        /// Дата с
        /// </summary>
        public DateTime? DateFrom { get; set; }
        
        /// <summary>
        /// Дата по
        /// </summary>
        public DateTime? DateTo { get; set; }
        
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? DateOfBirth { get; set; }
        
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// Список статусов заявки
        /// </summary>
        public List<CreditApplicationStatus> Statuses { get; set; }
        
        /// <summary>
        /// Тип анкеты
        /// </summary>
        public List<ProfileType> ProfileTypes { get; set; }
    }
}