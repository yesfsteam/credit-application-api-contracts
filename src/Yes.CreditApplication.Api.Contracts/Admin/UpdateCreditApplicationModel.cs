﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class UpdateCreditApplicationModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
     
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// E-mail клиента
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Срок кредита (в месяцах)
        /// </summary>
        public int? CreditPeriod { get; set; }
        
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int? CreditAmount { get; set; }
        
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? DateOfBirth { get; set; }
        
        /// <summary>
        /// Место рождения
        /// </summary>
        public string PlaceOfBirth { get; set; }
        
        /// <summary>
        /// Пол
        /// </summary>
        public Gender? Gender { get; set; }
        
        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string PassportSeries { get; set; }
        
        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string PassportNumber { get; set; }
        
        /// <summary>
        /// Кем выдан
        /// </summary>
        public string PassportIssuer { get; set; }
        
        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime? PassportIssueDate { get; set; }
        
        /// <summary>
        /// Код подразделения
        /// </summary>
        public string PassportDepartmentCode { get; set; }
        
        /// <summary>
        /// Регион прописки
        /// </summary>
        public string RegistrationAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город прописки
        /// </summary>
        public string RegistrationAddressCity { get; set; }
        
        /// <summary>
        /// Код города прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица прописки
        /// </summary>
        public string RegistrationAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом прописки
        /// </summary>
        public string RegistrationAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус прописки
        /// </summary>
        public string RegistrationAddressBlock { get; set; }
        
        /// <summary>
        /// Строение прописки
        /// </summary>
        public string RegistrationAddressBuilding { get; set; }
        
        /// <summary>
        /// Квартира прописки
        /// </summary>
        public string RegistrationAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса прописки по КЛАДР
        /// </summary>
        public string RegistrationAddressKladrCode { get; set; }
        
        /// <summary>
        /// Регион проживания
        /// </summary>
        public string ResidenceAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город проживания
        /// </summary>
        public string ResidenceAddressCity { get; set; }
        
        /// <summary>
        /// Код города проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица проживания
        /// </summary>
        public string ResidenceAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом проживания
        /// </summary>
        public string ResidenceAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус проживания
        /// </summary>
        public string ResidenceAddressBlock { get; set; }
        
        /// <summary>
        /// Строение проживания
        /// </summary>
        public string ResidenceAddressBuilding { get; set; }
        
        /// <summary>
        /// Квартира проживания
        /// </summary>
        public string ResidenceAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса проживания по КЛАДР
        /// </summary>
        public string ResidenceAddressKladrCode { get; set; }
        
        /// <summary>
        /// Образование
        /// </summary>
        public Education? Education { get; set; }

        /// <summary>
        /// Семейное положение
        /// </summary>
        public MaritalStatus? MaritalStatus { get; set; }
        
        /// <summary>
        /// Количество детей (иждивенцев)
        /// </summary>
        public int? DependentsCount { get; set; }
        
        /// <summary>
        /// Подтверждающий документ
        /// </summary>
        public ConfirmationDocument? ConfirmationDocument { get; set; }

        /// <summary>
        /// Наличие квартиры
        /// </summary>
        public bool HasFlat { get; set; }
        
        /// <summary>
        /// Наличие дома
        /// </summary>
        public bool HasHouse { get; set; }
        
        /// <summary>
        /// Наличие участка
        /// </summary>
        public bool HasArea { get; set; }
        
        /// <summary>
        /// Наличие автомобиля
        /// </summary>
        public bool HasCar { get; set; }

        /// <summary>
        /// Тип дополнительного телефона
        /// </summary>
        public AdditionalPhoneNumberType? AdditionalPhoneNumberType { get; set; }
        
        /// <summary>
        /// Дополнительный телефон
        /// </summary>
        public string AdditionalPhoneNumber { get; set; }
        
        /// <summary>
        /// Вид деятельности
        /// </summary>
        public Activity? Activity { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Tin { get; set; }
        
        /// <summary>
        /// Доход в месяц
        /// </summary>
        public int? MonthlyIncome { get; set; }
        
        /// <summary>
        /// Наименование работодателя
        /// </summary>
        public string EmployerName { get; set; }
        
        /// <summary>
        /// Регион работодателя
        /// </summary>
        public string EmployerAddressRegion { get; set; }
        
        /// <summary>
        /// Код региона работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressRegionKladrCode { get; set; }
        
        /// <summary>
        /// Город работодателя
        /// </summary>
        public string EmployerAddressCity { get; set; }
        
        /// <summary>
        /// Код города работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressCityKladrCode { get; set; }
        
        /// <summary>
        /// Улица работодателя
        /// </summary>
        public string EmployerAddressStreet { get; set; }
        
        /// <summary>
        /// Код улицы работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressStreetKladrCode { get; set; }
        
        /// <summary>
        /// Дом работодателя
        /// </summary>
        public string EmployerAddressHouse { get; set; }
        
        /// <summary>
        /// Корпус работодателя
        /// </summary>
        public string EmployerAddressBlock { get; set; }
        
        /// <summary>
        /// Строение работодателя
        /// </summary>
        public string EmployerAddressBuilding { get; set; }
        
        /// <summary>
        /// Офис работодателя
        /// </summary>
        public string EmployerAddressApartment { get; set; }
        
        /// <summary>
        /// Код адреса работодателя по КЛАДР
        /// </summary>
        public string EmployerAddressKladrCode { get; set; }

        /// <summary>
        /// Отрасль работодателя
        /// </summary>
        public EmployerIndustry? EmployerIndustry { get; set; }
        
        /// <summary>
        /// Численность работников
        /// </summary>
        public int? EmployerStaff { get; set; }
        
        /// <summary>
        /// Рабочий телефон
        /// </summary>
        public string EmployerPhoneNumber { get; set; }
        
        /// <summary>
        /// Дата начала работы на текущем месте
        /// </summary>
        public DateTime? EmployeeStartDate { get; set; }
        
        /// <summary>
        /// Должность
        /// </summary>
        public EmployeePosition? EmployeePosition { get; set; }

    }
}