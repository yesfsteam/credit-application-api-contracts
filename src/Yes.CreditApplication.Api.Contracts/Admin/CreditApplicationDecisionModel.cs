﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationDecisionModel
    {
        /// <summary>
        /// Статус решения по заявке на кредит
        /// </summary>
        public CreditApplicationStatus CreditApplicationStatus { get; set; }
        
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
        
        /// <summary>
        /// Название кредитной организации
        /// </summary>
        public string CreditOrganizationName { get; set; }
        
        /// <summary>
        /// Статус решения по заявке на кредит
        /// </summary>
        public CreditOrganizationRequestStatus CreditOrganizationStatus { get; set; }
        
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int? CreditAmount { get; set; }
        
        /// <summary>
        /// Срок кредита (в месяцах)
        /// </summary>
        public int? CreditPeriod { get; set; }
        
        /// <summary>
        /// Дата последнего обновления
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
        
        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Details { get; set; }
    }
}