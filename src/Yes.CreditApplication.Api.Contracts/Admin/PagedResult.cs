﻿using System.Collections.Generic;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class PagedResult<T>
    {
        public PagedResult(List<T> items, long total)
        {
            Items = items;
            Total = total;
        }

        /// <summary>
        /// Выборка элементов
        /// </summary>
        public List<T> Items { get; }

        /// <summary>
        /// Общее количество элементов коллекции
        /// </summary>
        public long Total { get; }
    }
}