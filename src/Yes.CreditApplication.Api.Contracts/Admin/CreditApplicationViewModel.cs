﻿using System;
using System.Collections.Generic;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationViewModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string MiddleName { get; set; }
        
        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// E-mail клиента
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int? CreditAmount { get; set; }
        
        /// <summary>
        /// Срок кредита (в месяцах)
        /// </summary>
        public int? CreditPeriod { get; set; }
        
        /// <summary>
        /// Дата заявки на кредит
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Название партнера
        /// </summary>
        public string PartnerName { get; set; }
        
        /// <summary>
        /// Статус заявки
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
        
        /// <summary>
        /// Шаг, до которого дошел клиент на лендинге
        /// </summary>
        public CreditApplicationStep Step { get; set; }
        
        /// <summary>
        /// Тип анкеты
        /// </summary>
        public ProfileType ProfileType { get; set; }
        
        /// <summary>
        /// Решения кредиторов по заявке на кредит
        /// </summary>
        public List<CreditApplicationDecisionModel> CreditApplicationDecisions { get; set; }
        
        /// <summary>
        /// История изменений заявки на кредит
        /// </summary>
        public List<CreditApplicationChangeModel> CreditApplicationChanges { get; set; }
    }
}