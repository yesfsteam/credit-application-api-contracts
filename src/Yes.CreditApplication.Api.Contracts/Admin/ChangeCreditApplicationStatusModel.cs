﻿using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class ChangeCreditApplicationStatusModel : JsonModel
    {
        /// <summary>
        /// Статус
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }
    }
}