﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationChangeModel
    {
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Статус заявки на кредит
        /// </summary>
        public CreditApplicationStatus Status { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }
    }
}