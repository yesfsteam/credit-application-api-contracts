﻿using System;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.Admin
{
    public class CreditApplicationRequest : JsonModel
    {
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
    }
}