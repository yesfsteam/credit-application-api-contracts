﻿using System;

namespace Yes.CreditApplication.Api.Contracts.CreditOrganizations
{
    public class ConfirmCreditApplicationRequest : CreditApplicationRequest
    {
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
    }
}