﻿using System;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.CreditOrganizations
{
    public class CreditApplicationStatusModel : JsonModel
    {
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
        
        /// <summary>
        /// Статус решения по заявке на кредит
        /// </summary>
        public CreditOrganizationRequestStatus Status { get; set; }
        
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public int? CreditAmount { get; set; }
        
        /// <summary>
        /// Срок кредита (в месяцах)
        /// </summary>
        public int? CreditPeriod { get; set; }
        
        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Details { get; set; }
    }
}