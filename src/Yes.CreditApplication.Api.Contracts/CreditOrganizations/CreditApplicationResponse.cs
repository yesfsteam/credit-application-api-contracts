﻿using System;
using System.Collections.Generic;
using Yes.Infrastructure.Common.Models;

namespace Yes.CreditApplication.Api.Contracts.CreditOrganizations
{
    public class CreditApplicationResponse : JsonModel
    {
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Данные по заявкам на кредит
        /// </summary>
        public List<CreditApplicationStatusModel> CreditApplications { get; set; }
    }
}