﻿using System.Net.Http;
using System.Threading.Tasks;
using Yes.Infrastructure.Http;

namespace Yes.CreditApplication.Api.Client
 {
     public interface IMaintenanceCreditApplicationClient
     {
         /// <summary>
         /// Удаляет неподтвержденные заявки на кредит
         /// </summary>
         Task<Response> DeleteExpiredCreditApplications();
     }

     public class MaintenanceCreditApplicationClient : RestClientBase, IMaintenanceCreditApplicationClient
     {
         public MaintenanceCreditApplicationClient(HttpClient client) : base(client, client.BaseAddress.ToString())
         {
         }

         public async Task<Response> DeleteExpiredCreditApplications()
         {
             return await Delete("api/v1/maintenance/credit-applications");
         }
   }
 }