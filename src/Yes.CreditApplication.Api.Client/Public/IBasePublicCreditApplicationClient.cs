﻿using System;
using System.Threading.Tasks;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace Yes.CreditApplication.Api.Client.Public
 {
     public interface IBasePublicCreditApplicationClient
     {
         /// <summary>
         /// Отправляет новый код подтверждения
         /// </summary>
         Task<Response<EmptyModel, ErrorModel>> ResendConfirmationCode(Guid creditApplicationId);
         
         /// <summary>
         /// Подтверждает номер телефона клиента
         /// </summary>
         Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, CreditApplicationConfirmationCodeModel model);
     }
 }