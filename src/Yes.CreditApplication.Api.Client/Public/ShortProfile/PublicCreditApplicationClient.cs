﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.CreditApplication.Api.Contracts.Public.ShortProfile;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace Yes.CreditApplication.Api.Client.Public.ShortProfile
 {
     public interface IPublicCreditApplicationClient : IBasePublicCreditApplicationClient
     {
         /// <summary>
         /// Создает заявку на кредит (Шаг 1)
         /// </summary>
         Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model);
     }

     public class PublicCreditApplicationClient : RestClientBase, IPublicCreditApplicationClient
     {
         public PublicCreditApplicationClient(HttpClient client) : base(client, client.BaseAddress.ToString())
         {
         }

         public async Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model)
         {
             return await Post<Guid>("api/v3/credit-applications", model);
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ResendConfirmationCode(Guid creditApplicationId)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v3/credit-applications/{creditApplicationId}/new-confirmation-code");
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, CreditApplicationConfirmationCodeModel model)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v3/credit-applications/{creditApplicationId}/confirmation-code", model);
         }
     }
 }