﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.CreditApplication.Api.Contracts.Public.MediumProfile;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace Yes.CreditApplication.Api.Client.Public.MediumProfile
 {
     public interface IPublicCreditApplicationClient : IBasePublicCreditApplicationClient
     {
         /// <summary>
         /// Создает заявку на кредит (Шаг 1)
         /// </summary>
         Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model);
         
         /// <summary>
         /// Сохраняет дополнительную информацию о клиенте (Шаг 3)
         /// </summary>
         Task<Response<EmptyModel, ErrorModel>> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model);
     }

     public class PublicCreditApplicationClient : RestClientBase, IPublicCreditApplicationClient
     {
         public PublicCreditApplicationClient(HttpClient client) : base(client, client.BaseAddress.ToString())
         {
         }

         public async Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model)
         {
             return await Post<Guid>("api/v2/credit-applications", model);
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ResendConfirmationCode(Guid creditApplicationId)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v2/credit-applications/{creditApplicationId}/new-confirmation-code");
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ConfirmPhoneNumber(Guid creditApplicationId, CreditApplicationConfirmationCodeModel model)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v2/credit-applications/{creditApplicationId}/confirmation-code", model);
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> SaveCreditApplicationAdditionalInformation(Guid creditApplicationId, CreditApplicationAdditionalInformationModel model)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v2/credit-applications/{creditApplicationId}/additional-information", model);
         }
     }
 }