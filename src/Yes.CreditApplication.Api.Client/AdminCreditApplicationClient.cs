﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.Admin.CreditApplicationRequest;

namespace Yes.CreditApplication.Api.Client
 {
     public interface IAdminCreditApplicationClient
     {
         /// <summary>
         /// Возвращает список заявок на кредит по заданному фильтру
         /// </summary>
         Task<Response<PagedResult<CreditApplicationListItemModel>>> GetCreditApplications(CreditApplicationListFilterModel model);

         /// <summary>
         /// Возвращает список заявок на кредит по заданному фильтру, для выгрузки в файл 
         /// </summary>
         Task<Response<List<CreditApplicationListItemExportModel>>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model);

         /// <summary>
         /// Возвращает информацию по заявке для просмотра
         /// </summary>
         Task<Response<CreditApplicationViewModel>> GetCreditApplicationViewModel(Guid creditApplicationId);
         
         /// <summary>
         /// Возвращает информацию по заявке для редактирования
         /// </summary>
         Task<Response<CreditApplicationEditViewModel>> GetCreditApplicationEditViewModel(Guid creditApplicationId);
         
         /// <summary>
         /// Возвращает список решений по заявке
         /// </summary>
         Task<Response<List<CreditApplicationDecisionModel>>> GetCreditApplicationDecisions(Guid creditApplicationId);
         
         /// <summary>
         /// Отправляет заново запрос на кредит
         /// </summary>
         Task<Response<EmptyModel, ErrorModel>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model);
         
         /// <summary>
         /// Подтверждает заявку на кредит
         /// </summary>
         Task<Response<EmptyModel, ErrorModel>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
         
         /// <summary>
         /// Обновляет статус заявки
         /// </summary>
         Task<Response> ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model);

         /// <summary>
         /// Обновляет информацию по заявке
         /// </summary>
         Task<Response> UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model);
     }

     public class AdminCreditApplicationClient : RestClientBase, IAdminCreditApplicationClient
     {
         public AdminCreditApplicationClient(HttpClient client) : base(client, client.BaseAddress.ToString())
         {
         }

         public async Task<Response<PagedResult<CreditApplicationListItemModel>>> GetCreditApplications(CreditApplicationListFilterModel model)
         {
             return await Get<PagedResult<CreditApplicationListItemModel>>("api/v1/credit-applications", model);
         }

         public async Task<Response<List<CreditApplicationListItemExportModel>>> GetCreditApplicationsForExport(CreditApplicationListFilterModel model)
         {
             return await Get<List<CreditApplicationListItemExportModel>>("api/v1/credit-applications/export", model);
         }

         public async Task<Response<CreditApplicationViewModel>> GetCreditApplicationViewModel(Guid creditApplicationId)
         {
             return await Get<CreditApplicationViewModel>($"api/v1/credit-applications/{creditApplicationId}");
         }
         
         public async Task<Response<CreditApplicationEditViewModel>> GetCreditApplicationEditViewModel(Guid creditApplicationId)
         {
             return await Get<CreditApplicationEditViewModel>($"api/v1/credit-applications/{creditApplicationId}/edit");
         }
         
         public async Task<Response<List<CreditApplicationDecisionModel>>> GetCreditApplicationDecisions(Guid creditApplicationId)
         {
             return await Post<List<CreditApplicationDecisionModel>, ErrorModel>($"api/v1/credit-applications/{creditApplicationId}/decisions");
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v1/credit-applications/{creditApplicationId}/retry", model);
         }
         
         public async Task<Response<EmptyModel, ErrorModel>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
         {
             return await Post<EmptyModel, ErrorModel>($"api/v1/credit-applications/{creditApplicationId}/confirmation", model);
         }
         
         public async Task<Response> ChangeCreditApplicationStatus(Guid creditApplicationId, ChangeCreditApplicationStatusModel model)
         {
             return await Put($"api/v1/credit-applications/{creditApplicationId}/status", model);
         }
         
         public async Task<Response> UpdateCreditApplication(Guid creditApplicationId, UpdateCreditApplicationModel model)
         {
             return await Put($"api/v1/credit-applications/{creditApplicationId}", model);
         }
     }
 }